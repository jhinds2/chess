package controller;
import models.*;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * 
 * This class represents and instance of a Chess game.
 * Normally two of these are made, the temporary board
 * and the visible end-user game.
 * 
 * @author David Martinez <dam364>
 * @author Justin Hinds <jth140>
 * 
 *
 */
public class Game {
	/**
	 * The Chess board.
	 */
    public ChessPiece[][] board;
    /**
     * The variable that determines whose turn it is in the game.
     * False means that it is white's turn.
     * True means that it is black's turn.
     */
    public boolean turnFlag;
    /**
     * Flag that determines whether the game has ended.
     */
    public boolean gameOver;
    /**
     * Utility that accepts user input.
     */
    public Scanner scanner;
    /**
     * Flag that determines whether this is the temporary board.
     * The flag changes some functionality.
     */
    public boolean isTemp;
    /**
     * Flag that determines whether someone has offered a draw.
     */
    public boolean setDraw; 
    /**
     * Flag that determines whether someone has accepted a draw.
     */
    public boolean acceptDraw;
    /**
     * Flag that determines which color player offered the draw.
     */
    public boolean hasSetDraw;
    /**
     * Flag that determines whether the game ended in a draw.
     */
    public boolean drawnGame;
    /**
     * Flag that determines whether the game should be output to the screen.
     */
    public boolean printGame;
    /**
     * Character variable that determines which castling (long or short)
     * was attempted.
     * Possible values include: 'l' - long (queen-side) castle
     * 							's' - short (king-side) castle
     * 							' ' - none
     */
    public char castleVar;
    /**
     * Flags that determine whether en passant is valid.
     * passantLeft determines whether a leftward en passant is valid.
     * passantRight determines whether a rightward en passant is valid.
     * moveTwo determines whether a turn has passed since the opportunity
     * to take en passant.
     */
    public boolean passantLeft, passantRight, moveTwo;
    /**
     * Flag that determines whether promotion is valid.
     */
    public boolean promotion;
    
    /**
     * Constructor for a Game.
     * This sets all default values for the user-facing game.
     */
    public Game() {
    	board = new ChessPiece[8][8];
        //False is white and true is black
        turnFlag = false;
        gameOver = false;
        scanner = new Scanner(System.in);
        printGame = true;
        isTemp = false;
        setDraw = false;
        acceptDraw = false;
        castleVar = ' ';
        drawnGame = false;
        passantLeft = false;
        passantRight = false;
        promotion = false;
    }

    /**
     * Sets the chess board variable up with the appropriate pieces.
     */
    public void setBoard() {
        board[0][0] = new Rook(false);
        board[0][1] = new Knight(false);
        board[0][2] = new Bishop(false);
        board[0][3] = new Queen(false);
        board[0][4] = new King(false);
        board[0][5] = new Bishop(false);
        board[0][6] = new Knight(false);
        board[0][7] = new Rook(false);
        for (int i=0; i <= 7; i++) {
            board[1][i] = new Pawn(false);
        }

        board[7][0] = new Rook(true);
        board[7][1] = new Knight(true);
        board[7][2] = new Bishop(true);
        board[7][3] = new Queen(true);
        board[7][4] = new King(true);
        board[7][5] = new Bishop(true);
        board[7][6] = new Knight(true);
        board[7][7] = new Rook(true);
        for (int i=0; i <= 7; i++) {
            board[6][i] = new Pawn(true);
        }
    }
    
    /**
     * Outputs the chess board to the console.
     */
    public void printBoard() {
        for (int i=7; i >= 0; i--) {
            for (int j=0; j <= 7; j++) {
                if (board[i][j] != null) {
                    System.out.print(board[i][j] + " ");
                } else {
                    if ((i + j) % 2 == 0) {
                        System.out.print("   ");
                    } else {
                        System.out.print("## ");
                    }
                }
                if (j == 7) {
                    System.out.print(i + 1 + "\n");
                }
            }
        }
        System.out.println(" a  b  c  d  e  f  g  h");
        System.out.println("");
    }

    /**
     * Prompts the player of a particular color for a move.
     * @return The input from the console
     */
    public String prompt() {
        if (turnFlag) {
            System.out.print("Black's move: ");
        } else {
            System.out.print("White's move: ");
        }
        String input = scanner.nextLine();
        return input;
    }

    /**
     * Parses through the input and returns tokens that are used
     * perform the move.
     * @param input Input provided by the user
     * @return A list of tokens that indicate position both of moving toward
     * and the piece selected, if the move warrants it.
     * Otherwise, null is returned if some other action is taken or the input
     * is found not to be permissible.
     */
    public ArrayList<String> chessTokens(String input) {
    	
        ArrayList<String> tokens = new ArrayList<String>();
        
        String piece;
        String moveTo = null;
        String thirdInput = null;
        
        boolean addDraw = false;
        boolean acptDraw = false;
        boolean isPromotion = false;

        try {
            StringTokenizer stok = new StringTokenizer(input);
            
            piece = stok.nextToken();
            
            //check for opponents draw
            if(setDraw && piece.equals("draw")) {
            	acptDraw = true;
            } else {
            moveTo = stok.nextToken();
            }
            
            //need to check here for draw or promotion
            if (stok.hasMoreTokens()) {
            	thirdInput = stok.nextToken().toLowerCase();
            	if(thirdInput.equals("draw?")) {
            		addDraw = true;
            	} else if(thirdInput.equals("q")) {
            		isPromotion = true;
            	} else if(thirdInput.equals("r")) {
            		isPromotion = true;
            	} else if(thirdInput.equals("b")) {
            		isPromotion = true;
            	} else if(thirdInput.equals("n")) {
            		isPromotion = true;
            	} else {
                return null;
            	}
            }
        } catch (NoSuchElementException e) {
            return null;
        }
        
        //this is a special case for the player accepting the draw
        if(setDraw && acceptDraw) {
        	//return null to avoid messing up make move
        	return null;
        }
        
        //if draw accepted
        if(acptDraw) {
        	tokens.add(piece);
        	return tokens;
        }

        tokens.add(piece);
        tokens.add(moveTo);
        
        //add draw input
        if(addDraw) {
        	tokens.add(thirdInput);
        }
        //add promotion input
        if(isPromotion) {
        	tokens.add(thirdInput);
        }
        
        return tokens;
    }
    
    /**
     * Checks whether the input is valid by checking whether valid tokens
     * were input, converting the token list and taking actions to determine
     * whether the move was just a basic move, or an attempt to promote a pawn,
     * or offer a draw.
     * @param input User input.
     * @return A boolean value showing whether or not the move was valid input.
     */
    public boolean isValidInput(String input) {
    	
        String piece;
        String moveTo;
        String thirdInput;
        
        
        ArrayList<String> tokens = new ArrayList<String>();
        tokens = chessTokens(input);

        if (tokens == null) { 
            return false;
        } 
        
        if(tokens.size() == 1) {
        	acceptDraw = true;
        	return false;
        } else {
        	setDraw = false;
        }
        	
    	//otherwise get inputs for move
        piece = tokens.get(0);
        moveTo = tokens.get(1);
                 
        if (piece.length() != 2 || moveTo.length() != 2) {
            return false;
        }
        if (!(piece.matches("[a-h][1-8]") && moveTo.matches("[a-h][1-8]"))) {
            return false;
        }
        try {
            int j = Character.getNumericValue(piece.charAt(0)) - 10;
            int i = Character.getNumericValue(piece.charAt(1)) - 1;
            int k = Character.getNumericValue(moveTo.charAt(1)) - 1;
            if (board[i][j] == null) {
                return false;
            } else {
                if (board[i][j].color != turnFlag) {
                    return false;
                }
            }
            //used for promotions
            if(board[i][j] instanceof Pawn && (k == 7 || k == 0)) {
            	promotion = true;
            }
        } catch (NullPointerException e) {
            return false;
        }
        
        //handle draw and promotion inputs
        if(tokens.size() == 3) {
        	thirdInput = tokens.get(2);
        	if(thirdInput.equals("draw?")) {
        		hasSetDraw = turnFlag;
            	setDraw = true;
            	return true;
        	}
        	if(!promotion) {
        		return false;
        	}
        }
          
        return true;
    }

    /**
     * Checks if the move is valid given the rules of chess for pieces.
     * The method does not check whether the move results in check for the player moving.
     * The method checks the instances of the pieces referenced.
     * It passes the pieces to overloaded methods in order to determine
     * the appropriate response.
     * @param input User input.
     * @return A boolean determination as to whether or not the move is a valid move
     * strictly in terms the logic for a particular piece.
     */
    public boolean isValidMove(String input) {
    	
    	ArrayList<String> tokens = new ArrayList<String>();
        tokens = chessTokens(input);
        
        if(tokens == null) {
        	return false;
        }
    	
        String piece = tokens.get(0);
        String moveTo = tokens.get(1);

        int j = Character.getNumericValue(piece.charAt(0)) - 10;
        int i = Character.getNumericValue(piece.charAt(1)) - 1;

        int l = Character.getNumericValue(moveTo.charAt(0)) - 10;
        int k = Character.getNumericValue(moveTo.charAt(1)) - 1;
        
        if (board[i][j] instanceof Pawn) {
        	return pieceLogic(i, j, (Pawn)board[i][j], k, l, board[k][l]);
        } else if (board[i][j] instanceof Knight){
        	return pieceLogic(i, j, (Knight)board[i][j], k, l, board[k][l]);
        } else if(board[i][j] instanceof Queen) {
        	return pieceLogic(i, j, (Queen)board[i][j], k, l, board[k][l]);
        } else if(board[i][j] instanceof King) {
        	return pieceLogic(i, j, (King)board[i][j], k, l, board[k][l]);
        } else if(board[i][j] instanceof Rook) {
        	return pieceLogic(i, j, (Rook)board[i][j], k, l, board[k][l]);
        } else if(board[i][j] instanceof Bishop) {
        	return pieceLogic(i, j, (Bishop)board[i][j], k, l, board[k][l]);
        } else {
        	return false;
        }
    }

    /**
     * An overloaded method that takes a piece of type ChessPiece, representing
     * an empty square. If this method is encountered, it should always fail,
     * as one cannot move empty squares.
     * @param i The row of the piece being moved.
     * @param j The column of the piece being moved.
     * @param piece The piece being moved.
     * @param k The row of the square that the piece is being moved to.
     * @param l The columns of the square that the piece is being moved to.
     * @param moveTo The square that the piece is being moved to.
     * @return A boolean determination as to whether or not the move is valid.
     */
    public boolean pieceLogic(int i, int j, ChessPiece piece, int k, int l, ChessPiece moveTo) {
        return false;
    }

    
    /**
     * Determines whether a move is a valid pawn move, overloaded.
     * Does not check if the move puts mover in check.
     * @param i The row of the piece being moved.
     * @param j The column of the piece being moved.
     * @param piece The piece being moved.
     * @param k The row of the square that the piece is being moved to.
     * @param l The columns of the square that the piece is being moved to.
     * @param moveTo The square that the piece is being moved to.
     * @return A boolean determination as to whether or not the move is valid.
     */
    public boolean pieceLogic(int i, int j, Pawn piece, int k, int l, ChessPiece moveTo) {
        //cannot move backwards
        if (piece.color) {
            if (i < k) {
                return false;
            }
        } else {
            if (i > k) {
                return false;
            }
        }
        
        //piece on moveTo, if exists, cannot be same color
        if (moveTo != null && moveTo.color == piece.color) {
        	return false;
        }
        
        //two rank first move
        if (!piece.hasMoved && moveTo == null) {
            if (!piece.color) {
                if (k - i == 2 && j - l == 0) {
                    if (board[k-1][l] == null) {
                        return true;
                    }
                }
            } else {
                if (i - k == 2 && j - l == 0) {
                    if (board[k+1][l] == null) {
                        return true;
                    }
                } 
            }
        } 
        
        //Move cannot be more than one row forward or one column off
        if (Math.abs(i - k) != 1 || Math.abs(j - l) > 1) {
            return false;
        }
        
        //Taking diagonally
        if (Math.abs(i - k) == 1 && Math.abs(j - l) == 1) {
        	//if a piece is there
        	if(moveTo != null) {
        		//set promotion if reaching end of board
        		if(k == 0 || k == 7) {
        			promotion = true;
        		} else {
        			promotion = false;
        		}
            return true;
        	}
            //en passant to right
        	if(j < 7) {
        		if(board[i][j+1] != null) {
        			if(board[i][j+1].enpassant) {
        			passantRight = true;
        			return true;
        			} else {
        				passantRight = false;
        			}
        		}
        	}
        	//en passant to left
        	if(j > 0) {
        		if(board[i][j-1] != null) {
        			if(board[i][j-1].enpassant) {
        			passantLeft = true;
        			return true;
        			} else {
        				passantLeft = false;
        			}
        		}
        	}
        }
        //Moving one forward
        if (Math.abs(i - k) == 1 && Math.abs( j - l) == 0 && moveTo == null) {
        	//set promotion if reaching end of board
    		if(k == 0 || k == 7) {
    			promotion = true;
    		} else {
    			promotion = false;
    		}
            return true;
        }
        
        return false;
    }
    
    /**
     * Determines whether the move is a valid move for a knight, overloaded.
     * Does not check whether the move puts the mover in check.
     * @param i The row of the piece being moved.
     * @param j The column of the piece being moved.
     * @param piece The piece being moved.
     * @param k The row of the square that the piece is being moved to.
     * @param l The columns of the square that the piece is being moved to.
     * @param moveTo The square that the piece is being moved to.
     * @return A boolean determination as to whether or not the move is valid.
     */
    public boolean pieceLogic(int i, int j, Knight piece, int k, int l, ChessPiece moveTo) {
    	
        //cannot move to square with same color piece
        if (moveTo != null && moveTo.color == piece.color) {
        	return false;
        }
        //must be three and one in any direction
        if (Math.abs(i - k) == 1 && Math.abs(j - l) == 2) {
        	return true;
        } else if (Math.abs(i - k) == 2 && Math.abs(j - l) == 1) {
        	return true;
        }
        return false;
    }


	/**
	 * Determines whether the move is a valid move for a rook, overloaded.
	 * Does not check whether the move puts the mover in check.
	 * @param i The row of the piece being moved.
     * @param j The column of the piece being moved.
     * @param piece The piece being moved.
     * @param k The row of the square that the piece is being moved to.
     * @param l The columns of the square that the piece is being moved to.
     * @param moveTo The square that the piece is being moved to.
     * @return A boolean determination as to whether or not the move is valid.
	 */
	public boolean pieceLogic(int i, int j, Rook piece, int k, int l, ChessPiece moveTo) {

		
		//don't let it move it move on to its own color
		if(moveTo != null && moveTo.color == piece.color) {
			return false;
		}
		
		//prevents it from moving diagonally
		if(Math.abs(i-k) >= 1 && Math.abs(j-l) >= 1) {
			return false;
		}
		
		//check for direction
		if(i > k) {
			//decrement to K and make sure no pieces are in the way
			for(i = i-1; i > k ; i--) {
				if(board[i][j] != null) {
					return false;
				}
			}	
		}
		
		//check for direction
		if(i < k) {
			//increment to K and make sure no pieces are in the way
			for(i = i+1; i < k ; i++) {
				if(board[i][j] != null) {
					return false;
				}
			}	
		}		
				
		//check for direction
		if(j > l) {
			//decrement to l and make sure no pieces are in the way
			for(j = j-1; j > l ; j--) {
				if(board[i][j] != null) {
					return false;
				}
			}	
		}
				
		//check for direction
		if(j < l) {
			//increment to l and make sure no pieces are in the way
			for(j = j+1; j  < l ; j++) {
				if(board[i][j] != null) {
					return false;
				}
			}	
		}
		
		return true;
	}
	
	/**
	 * Determines whether the move is a valid move for a bishop, overloaded.
	 * Does not check whether the move puts the mover in check.
	 * @param i The row of the piece being moved.
     * @param j The column of the piece being moved.
     * @param piece The piece being moved.
     * @param k The row of the square that the piece is being moved to.
     * @param l The columns of the square that the piece is being moved to.
     * @param moveTo The square that the piece is being moved to.
     * @return A boolean determination as to whether or not the move is valid.
	 */
	public boolean pieceLogic(int i, int j, Bishop piece, int k, int l, ChessPiece moveTo) {
		
		if(moveTo != null && moveTo.color == piece.color) {
			return false;
		}
		
		//diagonal check
		if(!(Math.abs(i-k) ==  Math.abs(j-l))) {
			return false;
		}
		
		//check for direction
		if(i > k && j > l) {
			//decrement to K, L and make sure no pieces are in the way
			for(i = i-1, j = j-1; i > k && j > l ; i--, j--) {
				if(board[i][j] != null) {
					return false;
				}
			}	
		}
				
		//check for direction
		if(i > k && j < l) {
			//decrement to K, increment to L and make sure no pieces are in the way
			for(i = i-1, j = j+1; i > k && j < l ; i--, j++) {
				if(board[i][j] != null) {
					return false;
				}
			}	
		}
							
						
		//check for direction
		if(i < k && j < l) {
			//increment to k and increment to L and make sure no pieces are in the way
			for(i = i+1, j = j+1; i < k && j < l ; i++, j++) {
				if(board[i][j] != null) {
					return false;
				}
			}	
		}
						
		//check for direction
		if(i < k && j > l) {
			//increment to k and decrement to L make sure no pieces are in the way
			for(i = i+1, j = j - 1; i < k && j > l ; i++, j--) {
				if(board[i][j] != null) {
					return false;
				}
			}	
		}
		
		
		return true;
	}
	
	/**
	 * Determines whether or not the move is a valid king move, overloaded.
	 * Castling is considered a king move, and as such, this handles the setting
	 * of the flag to ensure that castling occurs if it is attempted and is valid.
	 * Does not check whether the move puts the mover in check.
	 * @param i The row of the piece being moved.
     * @param j The column of the piece being moved.
     * @param piece The piece being moved.
     * @param k The row of the square that the piece is being moved to.
     * @param l The columns of the square that the piece is being moved to.
     * @param moveTo The square that the piece is being moved to.
     * @return A boolean determination as to whether or not the move is valid.
	 */
	public boolean pieceLogic(int i, int j, King piece, int k, int l, ChessPiece moveTo) {
		
		if(moveTo != null && moveTo.color == piece.color) {
			return false;
		}
		
		if(Math.abs(i - k) > 1 || Math.abs(j - l) > 1) {
			if(i == k && j == 4 && l == 6 && board[k][l + 1] instanceof Rook) {
				//Attempt to castle short
				if (!piece.hasMoved && !((Rook)board[k][l + 1]).hasMoved) {
					if (board[k][l - 1] == null && board[k][l] == null) {
						castleVar = 's';
						return true;
					}
				}
			} else if (i == k && j == 4 && l == 2 && board[k][l - 2] instanceof Rook) {
				//Attempt to castle long
				if (!piece.hasMoved && !((Rook)board[k][l - 2]).hasMoved) {
					if (board[k][l] == null && board[k][l + 1] == null) {
							castleVar = 'l';
							return true;
					}
				}
			}
			return false;
		}
		
		return true;
	}

	/**
	 * Determines whether the move is a valid move for a queen, overloaded.
	 * Does not check if the move puts the mover in check.
	 * @param i The row of the piece being moved.
     * @param j The column of the piece being moved.
     * @param piece The piece being moved.
     * @param k The row of the square that the piece is being moved to.
     * @param l The columns of the square that the piece is being moved to.
     * @param moveTo The square that the piece is being moved to.
     * @return A boolean determination as to whether or not the move is valid.
	 */
	public boolean pieceLogic(int i, int j, Queen piece, int k, int l, ChessPiece moveTo) {
		
		boolean isRook = false;
		boolean isBishop = false;
		
		if(moveTo != null && moveTo.color == piece.color) {
			 return false;
		}
		
		
		//bishop check
		if(Math.abs(i-k) ==  Math.abs(j-l)) {
			isBishop = true;
		}
		//Rook check
		if(!(Math.abs(i-k) >= 1 && Math.abs(j-l) >= 1)) {
			isRook = true;
		}
		//check if either bishop or rook
		if(!(isRook || isBishop)) {
			return false;
		}
		
		//bishop code-------------------------------------------------------------------------
		
		if(isBishop) {
			//check for direction
			if(i > k && j > l) {
			//decrement to K, L and make sure no pieces are in the way
				for(i = i-1, j = j-1; i > k && j > l ; i--, j--) {
					if(board[i][j] != null) {
						return false;
					}
				}	
			}
				
			//check for direction
			if(i > k && j < l) {
				//decrement to K, increment to L and make sure no pieces are in the way
				for(i = i-1, j = j+1; i > k && j < l ; i--, j++) {
					if(board[i][j] != null) {
					 	return false;
					}
				}	
			}					
						
			//check for direction
			if( i < k && j < l) {
				//increment to k and increment to L and make sure no pieces are in the way
				for(i = i+1, j = j+1; i < k && j < l ; i++, j++) {
					if(board[i][j] != null) {
						return false;
					}
				}	
		}
							
			//check for direction
			if(i < k && j > l) {
				//increment to k and decrement to L make sure no pieces are in the way
				for(i = i+1, j = j - 1; i < k && j > l ; i++, j--) {
					if(board[i][j] != null) {
						return false;
					}
				}	
			}
		}
		
		//rook code ------------------------------------------------------------------------	
		
		if(isRook) {		
		//check for direction
			if(i > k) {
			//decrement to K and make sure no pieces are in the way
				for(i = i-1; i > k ; i--) {
					if(board[i][j] != null) {
						return false;
					}
				}	
			}
				
			//check for direction
			if(i < k) {
				//increment to K and make sure no pieces are in the way
				for(i = i+1; i < k ; i++) {
					if(board[i][j] != null) {
						return false;
					}
				}	
			}

						
						
			//check for direction
			if(j > l) {
				//decrement to l and make sure no pieces are in the way
				for(j = j-1; j > l ; j--) {
					if(board[i][j] != null) {
						return false;
					}
				}	
			}
						
						
			//check for direction
			if(j < l) {
				//increment to l and make sure no pieces are in the way
				for(j = j+1; j  < l ; j++) {
					if(board[i][j] != null) {
						return false;
					}
				}	
			}
		}
		
		return true;
	}
	
	/**
	 * Changes that positions of the pieces on the board given that
	 * the move is found to be valid.
	 * It also performs the castling, en passant, and promotion as moves.
	 * @param input User input.
	 */
	public void makeMove(String input) {
		
		//parse input
		ArrayList<String> tokens = new ArrayList<String>();
        tokens = chessTokens(input);
        
        String piece = tokens.get(0);
        String moveTo = tokens.get(1);
        String promoteTo = null;
        
        //special case for promotion
        if(tokens.size() == 3 && promotion) {
        	promoteTo = tokens.get(2);
        }

        int j = Character.getNumericValue(piece.charAt(0)) - 10;
        int i = Character.getNumericValue(piece.charAt(1)) - 1;

        int l = Character.getNumericValue(moveTo.charAt(0)) - 10;
        int k = Character.getNumericValue(moveTo.charAt(1)) - 1;	
        
        if (castleVar == ' ' || !(board[i][j] instanceof King)) { 
	        //move piece
	        board[k][l] = board[i][j];
	        
	        //null old position
	        board[i][j] = null;
       } else if (castleVar == 's') {
    	   //move king into position
    	   board[k][l] = board[i][j];
    	   board[i][j] = null;
    	   
    	   //move rook next to king
    	   board[k][l - 1] = board[k][l + 1];
    	   board[k][l + 1] = null;
       } else if (castleVar == 'l') {
    	   board[k][l] = board[i][j];
    	   board[i][j] = null;
    	   
    	   board[k][l + 1] = board[k][l - 2];
    	   board[k][l - 2] = null;
       }
        
        //at this point the piece is at its new position
        //update has moved
        if(board[k][l] instanceof Pawn && !(isTemp)) {
        	
        	//for first move
        	if(!board[k][l].hasMoved) {
        	board[k][l].hasMoved = true;
        	//make sure it was a 2 space move for en passant
        		if(Math.abs(i - k) == 2) {
        			board[k][l].enpassant = true;
        		} else {
        			board[k][l].enpassant = false;
        		}
        	}
        	
        	//for en passant null the piece it passes
        	if(passantLeft && !turnFlag && board[k-1][l] instanceof Pawn) {
        		board[i][j-1] = null;
        		passantLeft = false;
        	} else if(passantLeft && turnFlag && board[k+1][l] instanceof Pawn) {
        		board[i][j-1] = null;
        		passantLeft = false;
        	} else if(passantRight && !turnFlag && board[k-1][l] instanceof Pawn) {
        		board[i][j+1] = null;
        		passantRight = false;
        	} else if(passantRight && turnFlag && board[k+1][l] instanceof Pawn) {
        		board[i][j+1] = null;
        		passantRight = false;
        	} else {
        		if(j < 7 && j > 0) {
        			if(board[i][j-1] != null) {
        			board[i][j-1].enpassant = false;
        			}
        			if(board[i][j+1] != null) {
        			board[i][j+1].enpassant = false;
        			}
        		}
        	}
        	
        	//handle promotions
        	if(promotion) {	
        		if(promoteTo == null || promoteTo.equals("q")) {
        			board[k][l] = new Queen(turnFlag);
        		} else if(promoteTo.equals("n")) {
        			board[k][l] = new Knight(turnFlag);
        		} else if(promoteTo.equals("b")) {
        			board[k][l] = new Bishop(turnFlag);
        		} else if(promoteTo.equals("r")) {
        			board[k][l] = new Rook(turnFlag);
        		} else {
        			//going to default to queen
        			board[k][l] = new Queen(turnFlag);
        		}
        		//reset promotion
        		//promotion = false;
        	}
        	
        }
        if(board[k][l] instanceof Rook && !(isTemp)) {
        	board[k][l].hasMoved = true;
        }
        if(board[k][l] instanceof King && !(isTemp)) {
        	board[k][l].hasMoved = true;
        }
        
		if (!isTemp) {
			castleVar = ' ';
		}
       
        //change turn
        turnFlag = !turnFlag;

	}

	/**
	 * A convenience function to turn a number back into a user-input
	 * style letter part of a coordinate.
	 * @param j A column number coordinate.
	 * @return The corresponding letter.
	 */
	public String numToLetter(int j) {
		String letter = "i";
		if (j == 0) {
			letter = "a";
		} else if (j == 1) {
			letter = "b";
		} else if (j == 2) {
			letter = "c";
		} else if (j == 3) {
			letter = "d";
		} else if (j == 4) {
			letter = "e";
		} else if (j == 5) {
			letter = "f";
		} else if (j == 6) {
			letter = "g";
		} else if (j == 7) {
			letter = "h";
		}
		return letter;
	}
	
	/**
	 * Checks whether the king of the player that just moved was placed into check.
	 * Also checks whether castling has attempted to move out of, into or through check.
	 * @return A boolean determination as to whether or not the king
	 * was placed into check.
	 */
	public boolean putsKingInCheck() {	
		boolean check = false;
		ArrayList<String> kingPosList = new ArrayList<String>();
		String piecePos;
		boolean correctColor = !turnFlag;
		int k = 0;
		int l = 0;
		
		if (!turnFlag) {
			if (castleVar == 's') {
				kingPosList.add("e8");
				kingPosList.add("f8");
			} else if (castleVar == 'l') {
				kingPosList.add("e8");
				kingPosList.add("d8");
			}
		} else {
			if (castleVar == 's') {
				kingPosList.add("e1");
				kingPosList.add("f1");
			} else if (castleVar == 'l') {
				kingPosList.add("e1");
				kingPosList.add("d1");
			}
		}
		
		for(int i = 0; i <= 7; i++) {
			for(int j = 0; j <=7; j++) {
				if (board[i][j] instanceof King) {
					if (board[i][j].color == !turnFlag) {
						kingPosList.add(numToLetter(j) + (i + 1));
						k = i;
						l = j;
						correctColor = board[k][l].color;
					}
				}
			}
		}
		
		for(int i = 0; i <= 7; i++) {
			for(int j = 0; j <= 7; j++) {
				piecePos = numToLetter(j) + (i + 1);
				for (String kingPos : kingPosList) {
					String moveToCheck = piecePos + " " + kingPos;
					//move king to simulated position
			        int n = Character.getNumericValue(kingPos.charAt(0)) - 10;
			        int m = Character.getNumericValue(kingPos.charAt(1)) - 1;
			        
			        //System.out.println(m + ", " + n);
			        
			        board[k][l] = null;
			        board[m][n] = new King(correctColor);
			        
					check = isValidMove(moveToCheck);
					
					board[m][n] = null;
					board[k][l] = new King(correctColor);
					
					if (check) {
						//System.out.println(moveToCheck);
						break;
					}
				}
				if (check) {
					break;
				}
			}
			if (check) {
				break;
			}
		}
		
		return check;
	}

	/**
	 * Checks if a player puts the opposing player into check.
	 * @return A boolean determination as to whether or not the opponent in now in check.
	 */
	public boolean inCheck() {
				
		boolean check = false;
		String kingPos = "";
		String piecePos;
			
		for(int i = 0; i <= 7; i++) {
			for(int j = 0; j <=7; j++) {
				if (board[i][j] instanceof King) {
					if (board[i][j].color == turnFlag) {
						kingPos = numToLetter(j) + (i + 1);
					}
				}
			}
		}
		
		for(int i = 0; i <= 7; i++) {
			for(int j = 0; j <= 7; j++) {
				piecePos = numToLetter(j) + (i + 1);
				String moveToCheck = piecePos + " " + kingPos;
				check = isValidMove(moveToCheck);
				if (check) {
					break;
				}
			}
			if (check) {
				break;
			}
		}
		
		return check;
	}
	
	/**
	 * A function that determines every possible valid move on the board strictly
	 * in terms of the movement of the pieces, not whether the player places 
	 * themselves in check.
	 * @return A list of strings of what would be considered valid user input.
	 */
	public ArrayList<String> getAllValidMoves() {
		ArrayList<String> validMoves = new ArrayList<String>();
		String piecePos = "";
		String moveTo = "";
		
		for(int i = 0; i <= 7; i++) {
			for(int j = 0; j <=7; j++) {
				for(int k = 0; k <= 7; k++) {
					for(int l = 0; l <= 7; l++) {
						if (board[i][j] != null &&
							board[i][j].color != turnFlag) {
							continue;
						}
						piecePos = numToLetter(j) + (i + 1);
						moveTo = numToLetter(l) + (k + 1);
						String moveToCheck = piecePos + " " + moveTo;
						if (isValidMove(moveToCheck)) {
							validMoves.add(moveToCheck);
						}
						castleVar = ' ';
					}
				}
		
			}
		}
		return validMoves;
	}
}
	




