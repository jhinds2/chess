package models;

/**
 * A pawn on a chess board. Subclass of ChessPiece.
 * @author David Martinez <dam364>
 * @author Justin Hinds <jth140>
 */
public class Pawn extends ChessPiece {
	/**
	 * The constructor for the pawn class that takes in a color.
	 * @param color The color of the piece, true is black and false is white.
	 */
	public Pawn(Boolean color) {
		//intialize chess piece
		pieceType = 'p';
		
		//set color
		this.color = color;
		this.hasMoved = false;
		this.numOfMoves = 0;
		this.enpassant = false;
		this.promoteTo = null;
		
	}

}
