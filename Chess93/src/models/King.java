package models;
/**
 * A king on a chess board. Subclass of ChessPiece.
 * @author David Martinez <dam364>
 * @author Justin Hinds <jth140>
 */
public class King extends ChessPiece {
	/**
	 * The constructor for the king class that takes in a color.
	 * @param color The color of the piece, true is black and false is white.
	 */
	public King(Boolean color) {
		
		//intialize chess piece
		pieceType = 'K';
		
		//set color
		this.color = color;
		this.hasMoved = false;
	}
	
}
