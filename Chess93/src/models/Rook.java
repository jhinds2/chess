package models;

/**
 * A rook on a chess board. Subclass of ChessPiece.
 * @author David Martinez <dam364>
 * @author Justin Hinds <jth140>
 */
public class Rook extends ChessPiece {
	/**
	 * The constructor for the rook class that takes in a color.
	 * @param color The color of the piece, true is black and false is white.
	 */
	public Rook(Boolean color) {
		
		//intialize chess piece
		pieceType = 'R';
		
		this.color = color;
		this.hasMoved = false;
	}

}
