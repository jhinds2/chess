package models;
/**
 * A queen on a chess board. Subclass of ChessPiece.
 * @author David Martinez <dam364>
 * @author Justin Hinds <jth140>
 */
public class Queen extends ChessPiece {
	/**
	 * The constructor for the queen class that takes in a color.
	 * @param color The color of the piece, true is black and false is white.
	 */
	public Queen(Boolean color) {
		//intialize chess piece
		pieceType = 'Q';
		
		//set color
		this.color = color;
	}

}
