package models;
/**
 * A knight on a chess board. Subclass of ChessPiece.
 * @author David Martinez <dam364>
 * @author Justin Hinds <jth140>
 */
public class Knight extends ChessPiece{
	/**
	 * The constructor for the knight class that takes in a color.
	 * @param color The color of the piece, true is black and false is white.
	 */
	public Knight(Boolean color) {
		//intialize chess piece
		pieceType = 'N';
		
		//set color
		this.color = color;
	}

}
