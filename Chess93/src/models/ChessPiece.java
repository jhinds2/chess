package models;
/**
 * The abstract class of all chess pieces, which determines common fields
 * and required functionality among all of them.
 * Furthermore, if this class is instantiated without occupation by a piece
 * then it is checked for a null value and treated as an empty square if it is null.
 * @author David Martinez <dam364>
 * @author Justin Hinds <jth140>
 */
public abstract class ChessPiece {
	/**
	 * The color of a piece.
	 * True is black and false is white.
	 */
	public boolean color;

	/**
	 * A character used in the toString of the piece.
	 * Is one of the following set:
	 * {'p' : pawn, 'R' : rook, 'N' : knight, 
	 * 'B' : bishop, 'Q' : queen, 'K' : king}
	 */
	public char pieceType;
	
	/**
	 * Flag to determine whether a piece has moved. Only utilized for
	 * pawns, rooks, and kings, whose movement options change once
	 * they have moved for the first time.
	 */
	public boolean hasMoved = false;
	
	/**
	 * The number of moves a piece has made. Used for pawns.
	 */
	public int numOfMoves;
	
	/**
	 * Flag to determine whether this piece (a pawn) has
	 * en passant available to them.
	 */
	public boolean enpassant;
	
	/**
	 * String to determine what the pawn should promote to.
	 */
	public String promoteTo;
	
	/**
	 * Represents the chess piece both for its class (pieceType) and for its color.
	 * Used in the display of the game board.
	 * @return A string representing the chess piece on the board.
	 */
	@Override
	public String toString() {
		
		//if color is black
		if(color) {
			return "b" + pieceType;
			
		}
		//if color is white
		else
			return "w" + pieceType;
		
	}
}
