package models;
/**
 * A bishop on a chess board. Subclass of ChessPiece.
 * @author David Martinez <dam364>
 * @author Justin Hinds <jth140>
 */
public class Bishop extends ChessPiece {
	/**
	 * The constructor for the bishop class that takes in a color.
	 * @param color The color of the piece, true is black and false is white.
	 */
	public Bishop(Boolean color) {
		//intialize chess piece
		pieceType = 'B';
		
		//set color
		this.color = color;
	}
}
