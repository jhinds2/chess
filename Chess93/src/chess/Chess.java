package chess;

import java.util.Arrays;
import controller.Game;

/**
 * The controller for games of chess. It handles ending games (draws or victories),
 * ensuring that moves are valid before making them on the board using the temporary
 * board, and using methods of the game class in order to display the board to the user
 * as they play.
 * @author David Martinez <dam364>
 * @author Justin Hinds <jth140>
 */
public class Chess {
	/**
	 * Main method that runs the instances of the Game class.
	 * @param args Any command-line input arguments, not used.
	 */
	public static void main(String[] args) {
		Game game = new Game();
		Game temp = new Game();
		game.setBoard();
        while(!game.gameOver) {
        	
        	if(game.printGame) {
        		game.printBoard();
        	}
        	
	        String input = game.prompt();
	        
	        //option to resign
	        if(input.equals("resign")) {
	        	game.turnFlag = !game.turnFlag;
	        	game.gameOver = true;
	        } else if(game.isValidInput(input) && game.isValidMove(input)) {
	        	
	    		for (int i = 0; i < game.board.length; i++) {
	    			temp.board[i] = Arrays.copyOf(game.board[i], game.board[i].length);
	    		}

	    		temp.turnFlag = game.turnFlag;
	    		temp.castleVar = game.castleVar;
	    		temp.isTemp = true;
	    		String temp_in = input;
	    		
	    		temp.makeMove(temp_in);
	    		if (temp.putsKingInCheck()) {
	    			game.printGame = false;
	    			game.castleVar = ' ';
	    			System.out.println("Illegal move, try again\n");
	    		} else {
	    			game.makeMove(input);
	    			game.printGame = true;
	    			//if (game.inCheck()) { System.out.println("Check"); }
	    			
	    			boolean hasValidMove = false;
	    			for (String move : game.getAllValidMoves()) {
	    				for (int i = 0; i < game.board.length; i++) {
	    	    			temp.board[i] = Arrays.copyOf(game.board[i], game.board[i].length);
	    	    		}
	    	    		
	    	    		temp.turnFlag = game.turnFlag;
	    	    		//sets castling flag
	    	    		temp.isValidMove(move);
	    	    		temp.makeMove(move);
	    	    		temp.castleVar = ' ';
	    	    		
	    	    		if (!temp.putsKingInCheck()) {
	    	    			if (game.inCheck())
	    	    			{ 
	    	    				System.out.println("Check");
	    	    			}
	    	    			hasValidMove = true;
	    	    			break;
	    	    		} else {
	    	    			hasValidMove = false;
	    	    		}
	    			}
	    			
	    			if (hasValidMove == false) {
	    				if(game.inCheck()) {
	    					System.out.println("Checkmate");
	    					game.turnFlag = !game.turnFlag;
	    					game.gameOver = true;
	    				} else {
	    					System.out.println("Stalemate");
	    					game.setDraw = true;
	    					game.gameOver = true;
	    					game.drawnGame = true;
	    				}
	    			}
	    		}
		    } else if (game.setDraw && game.acceptDraw) { 
	    		//end in draw
	    		game.drawnGame = true;
	    		game.gameOver = true;
	    	} else {
	    		//illegal move
	    		game.printGame = false;
	    		System.out.println("Illegal move, try again\n");
	    	}
        } //end while
        
        //print out winner
        if(!game.drawnGame) {
        	if (game.turnFlag) {
        		System.out.println("Black wins");
        	} else {
        		System.out.println("White wins");
        	}	
        } else {
        	System.out.println("draw");
        }
	}
}
